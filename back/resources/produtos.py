from flask import Blueprint,jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelProduto import Produto
from flask_jwt_extended import jwt_required

produtos = Blueprint('produtos', __name__)

@produtos.route('/produtos')
@cross_origin()
def listagemProdutos():
    produtos = Produto.query.order_by(Produto.id).all()
    return jsonify([produto.to_json() for produto in produtos])


@produtos.route('/produtos', methods=['POST'])
# Quando colocar o login descomentar essa linha
#@jwt_required
@cross_origin()
def cadastroproduto():
    produto = Produto.from_json(request.json)
    db.session.add(produto)
    db.session.commit()
    return jsonify(produto.to_json()), 201

    
@produtos.route('/produtos/<int:id>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
#@jwt_required()
@cross_origin()
def excluir(id):
    Produto.query.filter_by(id= id).delete()
    db.session.commit()
    return jsonify({'id':id,'message': 'Produto excluído com sucesso'}), 200

@produtos.route('/produtos/<int:id>', methods=['PUT'])
#@jwt_required()
@cross_origin()
def alterar(id):
    produto = Produto.query.get_or_404(id)

    produto.nserie = request.json['nserie']
    produto.marca = request.json['marca']
    produto.modelo = request.json['modelo']
    produto.descProduto = request.json['descProduto']
    produto.dataGarantia = request.json['dataGarantia']

    db.session.add(produto)
    db.session.commit()
    return jsonify(produto.to_json()),200


@produtos.route('/produto/<int:id>')
#@cross_origin()
def getByID(id):
    produto = Produto.query.get_or_404(id)
    return jsonify(produto.to_json()),200


@produtos.route('/produtos/<string:nome>')
#@cross_origin()
def getByNome(nome):
    produtos = Produto.query.order_by(Produto.id).filter(
    Produto.marca.like(f'%{nome}%') |
    Produto.modelo.like(f'%{nome}%')).all()
    #bolean testa se crodutos não existe
    if produtos:
        response = [produto.to_json() for produto in produtos]
        
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de crodutos para o formato JSON (list comprehensions)
    return jsonify(response)

@produtos.route('/produtos/<int:id>')
#@cross_origin()
def getById(id):
    produtos = Produto.query.order_by(Produto.id).filter(
    Produto.id.like(f'%{id}%')).all()
    #bolean testa se crodutos não existe
    if produtos:
        response = [produto.to_json() for produto in produtos]
        
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de crodutos para o formato JSON (list comprehensions)
    return jsonify(response)
    

