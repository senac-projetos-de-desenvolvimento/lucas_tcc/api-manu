from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelComponente import Componente
from flask_jwt_extended import jwt_required

componentes = Blueprint('componentes', __name__)

@componentes.route('/componentes')
@cross_origin()
def listagemComponentes():
    componentes = Componente.query.order_by(Componente.id).all()
    return jsonify([componente.to_json() for componente in componentes])


@componentes.route('/componentes', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastrocomponente():
    componente = Componente.from_json(request.json)
    db.session.add(componente)
    db.session.commit()
    return jsonify(componente.to_json()), 201


@componentes.route('/componentes/<int:id>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required()
@cross_origin()
def excluir(id):
    Componente.query.filter_by(id= id).delete()
    db.session.commit()
    return jsonify({'id':id,'message': 'Componente excluído com sucesso'}), 200

@componentes.route('/componentes/<int:id>', methods=['PUT'])
# @jwt_required()
@cross_origin()
def alterar(id):
    componente = Componente.query.get_or_404(id)

    componente.descricao = request.json['descricao']
    componente.valorComponente = request.json['valorComponente']
    componente.quantidade = request.json['quantidade']


    db.session.add(componente)
    db.session.commit()

    return jsonify(componente.to_json()),200
@componentes.route('/componentesQtd/<int:id>', methods=['PUT'])
# @jwt_required()
@cross_origin()
def ajustar(id):
    componente = Componente.query.get_or_404(id)
    componente.quantidade = request.json['qtd']
    db.session.add(componente)
    db.session.commit()
    return jsonify(componente.to_json()),200


@componentes.route('/componentes/<int:id>')
#@cross_origin()
def getByID(id):
    componente = Componente.query.get_or_404(id)
    return jsonify(componente.to_json()),200


@componentes.route('/componentes/<string:desc>')
#@cross_origin()
def getByNome(desc):
    componentes = Componente.query.order_by(Componente.id).filter(
    Componente.descricao.like(f'%{desc}%')).all()
    #bolean testa se componentes não existe
    if componentes:
        response = [componente.to_json() for componente in componentes]
        
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de componentes para o formato JSON (list comprehensions)
    return jsonify(response)
    

