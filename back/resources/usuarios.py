from flask import Blueprint, jsonify, request
from flask_cors import CORS, cross_origin
from flask_jwt_extended import create_access_token,  jwt_required, get_jwt
from banco import db
from models.modelTecnico import Tecnico
from models.modelUsuario import Usuario
from models.modelCliente import Cliente
from config import config
import hashlib
from blocklist import blocklist
from resources.tecnicos import acess

usuarios = Blueprint('usuarios', __name__)

# testar se não tem que fazer individual por função
# @jwt_required()
# def access():
#     claims = get_jwt()
#     access=claims["access"]
#     return access

@usuarios.route('/usuarios')
def list_user():
    usuarios = Usuario.query.order_by(Usuario.nome).all()
    return jsonify([usuario.to_json() for usuario in usuarios])

#fazer este post apenas pelo banco 
#ou outra forma de validar pois a rota é insegura
#o post do primeiro usuario é feita sem token
@usuarios.route('/usuarios', methods=['POST'])
def includ_user():
    usuario = Usuario.from_json(request.json)
    db.session.add(usuario)
    db.session.commit()
    return jsonify(usuario.to_json()), 201

# login
# --------------------------------------------
@usuarios.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    email = request.json.get('email', None)
    senha = request.json.get('senha', None)
    if not email:
        return jsonify({"msg": "Missing email parameter"}), 400
    if not senha:
        return jsonify({"msg": "Missing senha parameter"}), 400

    senha += config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()

    usuario = Usuario.query \
        .filter(Usuario.email == email) \
        .filter(Usuario.senha == senha_md5) \
        .first()

    if usuario :
        # passando parametro adicional dentro do token
        permission = usuario.access
        user_id = usuario.id
        additional_claims = {"access": permission, "user_id": user_id }
        access_token = create_access_token(identity=email, additional_claims=additional_claims)
        if usuario.flag==0:
            return jsonify({"user": 0, "access_token": None}), 200
        else:
            return jsonify({"user": usuario.nome, "access_token": access_token, "permission":permission}), 200
    else:
        return jsonify({"user": None, "access_token": None}), 200

@usuarios.route('/inative/<int:id>', methods=['PUT'])
# @jwt_required()
@cross_origin()
def inativeUser(id):
    user = Usuario.query.get_or_404(id)
    if user.flag == 1:
        user.flag = 0
    else:
        user.flag = 1
        
    db.session.commit()
    db.session.add(user)
    return jsonify({'id':id,'message': 'Tecnico inativado com sucesso'}), 200

# cliente
# ---------------------------------------------#

@usuarios.route('/clientes', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
def addClient():
    db.session.begin()
    try:
        usuario = Usuario.from_json(request.json)
        usuario.access = 3
        db.session.add(usuario)
        db.session.commit()
        #com o retorno do json faz o post na tabela Cliente
        cliente = Cliente.from_json(usuario.to_json_cliente(request.json))
        db.session.add(cliente)
        db.session.commit()
        return jsonify(cliente.to_json()), 201
    except Exception:
        db.session.rollback()
        return jsonify(cliente.to_json()), 501

@usuarios.route('/clientes/<int:id>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
def deleteClient(id):
    Cliente.query.filter_by(id= id).delete()
    db.session.commit()
    return jsonify({'id':id,'message': 'cliente excluído com sucesso'}), 200

@usuarios.route('/clientes/<int:id>', methods=['PUT'])
# @jwt_required
def alterClient(id):
    user = Usuario.query.get_or_404(id)
    user.nome = request.json['nome']
    user.email = request.json['email']
    user.cpf = request.json['cpf']
    #necessário encriptar aqui pois não passa pelo json do model
    senha = request.json['senha'] + config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()
    user.senha = senha_md5

    cliente = Cliente.query.filter_by(user_id=user.id).first()

    
    cliente.telefone = request.json['telefone']
    cliente.endereco = request.json['endereco']
    cliente.valor = request.json['valor']

    db.session.add(cliente)
    db.session.add(user)
    db.session.commit()
    return jsonify(cliente.to_json()),200

# Tecnico
# ------------------------------------------------
@usuarios.route('/tecnicos', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required()
@cross_origin()
def addTech():
    db.session.begin()
    try:
        #faz o post primeiro na tabela Usuario
        usuario = Usuario.from_json(request.json)
        usuario.access = 2
        db.session.add(usuario)
        db.session.commit()
        #com o retorno do json faz o post na tabela Tecnico
        tecnico = Tecnico.from_json(usuario.to_json_tecnico(request.json))
        db.session.add(tecnico)
        db.session.commit()
        return jsonify(tecnico.to_json()), 201
    except Exception:
        db.session.rollback()
        return jsonify(tecnico.to_json()), 501

@usuarios.route('/tecnicos/<int:id>', methods=['PUT'])
# @jwt_required()
@cross_origin()
def alterTech(id):

    user = Usuario.query.get_or_404(id)
    user.nome = request.json['nome']
    user.email = request.json['email']
    user.cpf = request.json['cpf']

    #necessário encriptar aqui pois não passa pelo json do model
    senha = request.json['senha'] + config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()
    user.senha = senha_md5
    #após alterar na tabela usuario busca na tabela tecnico o primeiro através da foreingkey
    tecnico = Tecnico.query.filter_by(user_id=user.id).first()

    tecnico.telefone = request.json['telefone']
    
    
    db.session.add(tecnico)
    db.session.add(user)
    db.session.commit()
    return jsonify(tecnico.to_json()),200

@usuarios.route('/tecnicos/<int:id>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
@jwt_required()
@cross_origin()
def deleteTech(id):
    Tecnico.query.filter_by(id= id).delete()
    db.session.commit()
    return jsonify({'id':id,'message': 'Tecnico excluído com sucesso'}), 200

# adiciona o token a um lista de token invalidos
@usuarios.route('/logout')
@jwt_required()
def logout():
    jti = get_jwt()['jti']
    blocklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200

