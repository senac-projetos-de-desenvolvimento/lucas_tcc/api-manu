from flask import Blueprint,jsonify
from banco import db
from flask_cors import CORS, cross_origin
from models.modelTecnico import Tecnico
from flask_jwt_extended import create_access_token, jwt_required, get_jwt
from blocklist import  blocklist
from models.modelUsuario import Usuario

tecnicos = Blueprint('tecnicos', __name__)

@jwt_required()
def acess():
    claims = get_jwt()
    acess=claims["access"]
    return acess

@tecnicos.route('/tecnicos')
# @jwt_required()
@cross_origin()
def listagemtecnicos():
    
    # if acess() == 2 :
        tecnicos = Tecnico.query.order_by(Tecnico.id).all()
        return jsonify([tecnico.to_json() for tecnico in tecnicos])
    # else :
    #     return jsonify("access denied")
@tecnicos.route('/tecnicoedit')
@jwt_required()
@cross_origin()
def tecnicoEdit():
    claims = get_jwt()
    user_id=claims["user_id"]
    print("user",user_id)
    tecnico = Tecnico.query.order_by(Tecnico.id).filter_by(user_id = user_id).first()
    return jsonify(tecnico.to_json()),200

@tecnicos.route('/tecnicos/<int:id>')
# @jwt_required()
@cross_origin()
def getByID(id):
    tecnico = Tecnico.query.get_or_404(id)
    return jsonify(tecnico.to_json()),200

@tecnicos.route('/tecnicos/<string:nome>')
# @jwt_required()
@cross_origin()
def getByNome(nome):
    tecnicos = Tecnico.query.order_by(Tecnico.id).join(Usuario).filter(
    Usuario.email.like(f'%{nome}%') |
    Usuario.nome.like(f'%{nome}%')).all()

    # tecnicos = Tecnico.query.order_by(Tecnico.id).filter(
    # Usuario.email.like(f'%{nome}%') |
    # Usuario.nome.like(f'%{nome}%')).all()

    #bolean testa se tecnicos não existe
    if tecnicos:
        response = [tecnico.to_json() for tecnico in tecnicos]
        
    else:
        response={'erro': 'nenhum registro anterior encontrado'}
    # converte a lista de filmes para o formato JSON (list comprehensions)
    return jsonify(response)
    
@tecnicos.route('/logout')
@jwt_required()
def logout():
    jti = get_jwt()['jti']
    blocklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200
