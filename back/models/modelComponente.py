from banco import db

class Componente(db.Model):
    __tablename__ = 'componentes'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    descricao = db.Column(db.String(250), nullable=False)
    valorComponente = db.Column(db.Float(9.4), nullable=False)
    quantidade = db.Column(db.Float(9.4), nullable=False)


#envia
    def to_json(self):
        json_componentes = {
            'id': self.id,
            'descricao': self.descricao,
            'valorComponente': self.valorComponente,
            'quantidade': self.quantidade,

        }
        return json_componentes
#recebe
    @staticmethod
    def from_json(json_componentes):
        descricao = json_componentes.get('descricao')
        valorComponente = json_componentes.get('valorComponente')
        quantidade = json_componentes.get('quantidade')

        return Componente(descricao=descricao,quantidade=quantidade, valorComponente=valorComponente)
