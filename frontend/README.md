# Assistencia Técnica

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### Install date picker
```
npm i vue2-datepicker
```
### Install date formater
```
npm install moment
```
### Install vue-select to dropdown menu
```
npm install vue-select
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
