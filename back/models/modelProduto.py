from banco import db

class Produto(db.Model):
    __tablename__ = 'produtos'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    nserie = db.Column(db.String(20), nullable=False)
    marca = db.Column(db.String(250), nullable=False)
    modelo = db.Column(db.String(250), nullable=False)
    descProduto = db.Column(db.String(250), nullable=False)
    #data que expira a garantia, mesmo valor do dataWaranty do modelChamado
    dataGarantia = db.Column(db.String(250), nullable=True)

    chamados = db.relationship('Chamado')

    def to_json(self):
        json_produtos = {
            'id': self.id,
            'nserie': self.nserie,
            'marca': self.marca,
            'modelo': self.modelo,
            'descProduto': self.descProduto,
            'dataGarantia': self.dataGarantia,

        }
        return json_produtos

    @staticmethod
    def from_json(json_produtos):
        nserie = json_produtos.get('nserie')
        marca = json_produtos.get('marca')
        modelo = json_produtos.get('modelo')
        descProduto = json_produtos.get('descProduto')
        dataGarantia = json_produtos.get('dataGarantia')

        return Produto(nserie=nserie,modelo=modelo, marca=marca,descProduto = descProduto, dataGarantia = dataGarantia)
